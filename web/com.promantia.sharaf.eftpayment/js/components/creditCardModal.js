/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, $ */

enyo.kind({
  name: 'OB.UI.IntegratedCreditCard',
  kind: 'OB.UI.ModalAction',
  hideCloseButton: true,
  header: 'Process Payment:',
  autoDismiss: false,
  events: {
    onHideThisPopup: ''
  },
  handlers: {
    onApplyChanges: 'applyChanges'
  },
  executeOnHide: function() {
	  if(!OB.UTIL.isNullOrUndefined(this.args.callbacks)){
			 OB.UTIL.HookManager.callbackExecutor(this.args.args, this.args.callbacks);  
		  }
	  },
  executeOnShow: function() {
	  var amount = 0;
	  var name;
	  if(!OB.UTIL.isNullOrUndefined(this.args.args) && !OB.UTIL.isNullOrUndefined(this.args.args.providerGroup.provider.cpseftProviderCode) && this.args.args.providerGroup.provider.cpseftProviderCode === 'integrated.snbaubank'){
		  if(this.hideCloseButton){
			  this.$.headerCloseButton.show();
			  }
		  amount = this.args.args.amount;
		  name = this.args.args.providerGroup.provider.name;
		  this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardAuthCodeIc.hide();
	  }else{
		  amount = this.args.args.paymentToAdd.get('amount');
		  name = this.args.args.paymentToAdd.get('paymentData').provider.name;
	  }
    this.$.header.setContent(OB.I18N.getLabel('OBPOS_LblModalPayment', [
            OB.I18N.formatCurrency(amount)
          ]));
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.lblTypeIc.setContent(OB.I18N.getLabel('OBPOS_LblModalType'));
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.lblAmountIc.setContent(OB.I18N.getLabel('OBPOS_LblModalAmount'));
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.lblCardNumberIc.setContent(OB.I18N.getLabel('SHARCC_CardNumber'));
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.lblCardPadIc.setContent('******');
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.paymenttypeIc.setContent(name);
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.paymentamountIc.setContent(OB.I18N.formatCurrency(amount));
    
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber6Ic.setValue('');
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber4Ic.setValue('');
    if(!OB.UTIL.isNullOrUndefined(this.args.args.paymentToAdd)){
        this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.lblCardAuthCodeIc.setContent(OB.I18N.getLabel('SHARCC_CardAuthCode'));
        this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardAuthCodeIc.setValue('');
    	 this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.errorIc.setContent(OB.I18N.getLabel('CPSEFT_CardDetailsMissing', [name
    		  ]));	
    }
  },
  bodyContent: {components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
    	kind: 'OB.UI.IntegratedCCButton'
  	}]
  },
  applyChanges: function () {
	  var originalCreditCardNum = null;
	  var originalApprovalCode = null;
	  var me = this;
    var cardNumber6Ic = this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber6Ic.getValue(),
        cardNumber4Ic = this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber4Ic.getValue(),
        cardAuthCodeIc = this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardAuthCodeIc.getValue();
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber6Ic.setStyle('float: left; width: 90px; margin-bottom: 0px');
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber4Ic.setStyle('float: left; width: 60px; margin-bottom: 0px');
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardAuthCodeIc.setStyle('float: left; width: 238px; margin-bottom: 0px');
    this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.errorIc.setContent('');

    if (cardNumber6Ic === '' || cardNumber6Ic.length !== 6) {
      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber6Ic.setStyle('float: left; width: 90px; margin-bottom: 0px; border: 1px solid red;');
      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.errorIc.setContent(OB.I18N.getLabel('SHARCC_ErrorCardNumber6'));
      return;
    } else {
      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber6Ic.setValue(OB.UTIL.padNumber(cardNumber6Ic, 6));
    }
    if (cardNumber4Ic === '' || cardNumber4Ic.length !== 4) {
      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber4Ic.setStyle('float: left; width: 60px; margin-bottom: 0px; border: 1px solid red;');
      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.errorIc.setContent(OB.I18N.getLabel('SHARCC_ErrorCardNumber4'));
      return;
    } else {
      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardNumber4Ic.setValue(OB.UTIL.padNumber(cardNumber4Ic, 4));
    }
    
      var cardNumber = OB.UTIL.padNumber(cardNumber6Ic, 6) + '******' + OB.UTIL.padNumber(cardNumber4Ic, 4);
	  if(!OB.UTIL.isNullOrUndefined(this.args.args) && !OB.UTIL.isNullOrUndefined(this.args.args.providerGroup.provider.cpseftProviderCode) && this.args.args.providerGroup.provider.cpseftProviderCode === 'integrated.snbaubank'){
		  _.each(me.args.args.receipt.get('originalPayments'), function(payment) {
		       if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.properties)) {
		    	   originalCreditCardNum = payment.paymentData.properties.cardNumber;
		           if(originalCreditCardNum === cardNumber){
		        	    me.doHideThisPopup();
		        	    me.args.me.startPaymentRefund();
		           }else{
		        	   OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'),OB.I18N.getLabel('CPSEFT_AUrefundcardvalidation'));
		   	         return;
		           }
		       }		       
		  });		  
	  }
	  if(!OB.UTIL.isNullOrUndefined(this.args.args)  && !OB.UTIL.isNullOrUndefined(this.args.args.paymentToAdd)){
		  if (cardAuthCodeIc === '' || cardAuthCodeIc.length > 30) {
		      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.cardAuthCodeIc.setStyle('float: left; width: 238px; margin-bottom: 0px; border: 1px solid red;');
		      this.$.bodyContent.$.attributes.$.line_creditCardIcAttr.$.errorIc.setContent(OB.I18N.getLabel('SHARCC_ErrorCardAuthCode'));
		      return;
		    }
	      this.args.args.paymentToAdd.set('eftCardDetails', cardNumber);
	      this.args.args.paymentToAdd.set('eftCardApprovalCode', cardAuthCodeIc);	      
	      if (OB.UTIL.isNullOrUndefined(this.args.args.paymentToAdd.get('paymentData').properties)) {
	        var properties;
	        properties.cardNumber = cardNumber;
	        this.args.args.paymentToAdd.get('paymentData').properties = properties;
	      } else {
	      	this.args.args.paymentToAdd.get('paymentData').properties.cardNumber = cardNumber;
	      } 
	      this.doHideThisPopup();
	  }    
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.CreditCardIcAttr',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'OB.UI.CreditCardIcAttr',
  components: [{
      classes: 'row-fluid',
      components: [{
        style: 'float:left; padding-left:30px',
        name: 'lblTypeIc'
      }, {
        name: 'paymenttypeIc',
        style: 'float:right; font-weight: bold; padding-right:30px'
      }]
    }, {
      style: 'clear: both'
    }, {
      classes: 'row-fluid',
      components: [{
        style: 'float:left; padding-left:30px',
        name: 'lblAmountIc'
      }, {
        name: 'paymentamountIc',
        style: 'float:right; font-weight: bold; padding-right:30px'
      }, {
        style: 'clear: both; padding: 5px;'
      }, {
        classes: 'row-fluid',
        components: [{
          style: 'float: left; padding-left: 30px; min-width: 252px; text-align: left;',
          name: 'lblCardNumberIc'
        }, {
          name: 'cardNumber6Ic',
          kind: 'OB.UI.EditNumber',
          type: 'text',
          classes: 'input',
          value: '',
          style: 'float: left; width: 90px; margin-bottom: 0px'
        }, {
          style: 'float: left; padding-left: 15px; padding-right: 15px',
          name: 'lblCardPadIc'
        }, {
          name: 'cardNumber4Ic',
          kind: 'OB.UI.EditNumber',
          type: 'text',
          classes: 'input',
          value: '',
          style: 'float: left; width: 60px; margin-bottom: 0px'
        }]
      }, {
        style: 'clear: both; padding: 5px;'
      }, {
        classes: 'row-fluid',
        components: [{
          style: 'float: left; padding-left: 30px; min-width: 252px; text-align: left;',
          name: 'lblCardAuthCodeIc'
        }, {
          name: 'cardAuthCodeIc',
          kind: 'enyo.Input',
          type: 'text',
          classes: 'input',
          style: 'float: left; width: 238px; margin-bottom: 0px'
        }]
      }]
    }, {
      style: 'clear: both; padding: 10px;'
    }, {
      style: 'clear: both; padding: 10px; color: red;',
      name: 'errorIc'
    }, {
      style: 'clear: both; padding: 10px;'
    }],
  initComponents: function () {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: 'OB.UI.IntegratedCreditCardImpl',
  kind: 'OB.UI.IntegratedCreditCard',
  newAttributes: [{
    kind: 'OB.UI.CreditCardIcAttr',
    name: 'creditCardIcAttr'
  }]
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.IntegratedCCButton',
  style: 'float: right;',
  i18nContent: 'OBMOBC_LblOk',
  isDefaultAction: true,
  events: {
    onApplyChanges: ''
  },
  tap: function () {
    this.owner.owner.applyChanges();
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'OB.UI.IntegratedCreditCardImpl',
  name: 'OB_UI_IntegratedCreditCard'
});
