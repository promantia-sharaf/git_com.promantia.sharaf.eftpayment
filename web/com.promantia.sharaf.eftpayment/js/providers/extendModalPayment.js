OB.OBPOSPointOfSale.UI.Modals.ModalPaymentsSelect.prototype.searchAction = function() {
    var items = [],
    payments = OB.POS.modelterminal.get('payments'),
    filterBy = this.$.body.$.paymentname.getValue().toUpperCase();
  if (this.args.availables) {
    enyo.forEach(
      this.args.availables,
      function(sk) {
        var payment = _.find(payments, function(pay) {
          return pay.paymentMethod.searchKey === sk;
        });
        if (payment) {
          items.push({
            name: payment.paymentMethod._identifier,
            image: payment.image,
            payment: payment,
            disabled: false
          });
        }
      },
      this
    );
  } else {
    enyo.forEach(
      payments,
      function(payment) {
        if (
          payment.paymentMethod.paymentMethodCategory &&
          payment.paymentMethod.paymentMethodCategory ===
            this.args.idCategory &&
          OB.MobileApp.model.hasPermission(payment.payment.searchKey)
        ) {
          if (
            filterBy === '' ||
            payment.paymentMethod._identifier
              .toUpperCase()
              .indexOf(filterBy) >= 0
          ) {
            var isDisabled =
              OB.MobileApp.model.receipt.getTotal() < 0
                ? !payment.paymentMethod.refundable
                : false;
            //to add payment methods other than NI payment to the credit card popup
            if(!OB.UTIL.isNullOrUndefined(payment.providerGroup)){
               // dont show payment method
            }else{
                items.push({
                    name: payment.paymentMethod._identifier,
                    image: payment.image,
                    payment: payment,
                    disabled: isDisabled
                  });
            }
          }
        }
      },
      this
    );
  }
  this.$.body.$.paymentMethods.setItems(items);
  var me = this;
  setTimeout(function() {
    me.$.body.$.paymentMethods.render();
  }, 1);

};