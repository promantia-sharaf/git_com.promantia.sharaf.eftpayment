// ****************** extending OB.UI.ModalProviderGroup ***************************** //
/*global $, _ , OBPOS_StandardProvider*/

OB.UI.ModalProviderGroup.extend({
  executeOnShow: function() {
    var receipt = this.args.receipt;
    var amount = this.args.amount;
    var refund = this.args.refund;
    var providerGroup = this.args.providerGroup;
    var remainingRefundAmt,refundAmt;
    var isOverPaymnet =  false;
    var paymnetKindDuringSale = null;
    var refundedAmtSnb = 0;
    var alredyAddedAmtInPresentReceipt = 0;
    var me = this;
    
    if (receipt.get('custDisBankBinPromotionFlowInProgress')) {
      this.hide();
      return false;
    }
    
    if (!OB.UTIL.isNullOrUndefined(this.args.receipt.get('custdisBankBinValidated')) && !this.args.receipt.get('custdisBankBinValidated')) {
      this.hide();
      var me = this;
      this.args.order = this.args.receipt;
      OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
         popup: 'OB_UI_ModalCardBinNumber',
         args: {
             promotions: me.args.receipt.get('custDisBankBinPromotion'),
             args: me.args,
             order: me.args.order,
             callbacks: me.args.callbacks 
      }});
      return false; 
    }
    var discount = 0.0;
    if (receipt.get('custdisBankBinDiscountApplied')) {
      var custdisSelectedLineAmount = receipt.get('custdisSelectedLineAmount');
      var grossAmt = amount;

      if (receipt.get('custdisMinPurchaseAmountBankBin') > grossAmt) {
         var symbol = OB.MobileApp.model.get('terminal').symbol;
         var symbolAtRight = OB.MobileApp.model.get('terminal').currencySymbolAtTheRight;
         OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningTitle'), OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningBody', [OB.I18N.formatCurrencyWithSymbol(receipt.get('custdisMinPurchaseAmountBankBin'), symbol, symbolAtRight)]));
         return false;
      }
      var discountAmount = new Array();
      var prodCatDiscount = false;
      var cumulativeAmount = new Array();
      var apportionedAmount = new Array();
      var apportionedCumulative = new Array();
      var discountCumulative = new Array();     
      var prodCatDiscount = false;
      var invoiceQtyProductLimit = 0;
      if(!OB.UTIL.isNullOrUndefined(receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty')) && receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty') < receipt.get('custDisBankBinPromotion').get('custdisProductLines').length){
    	  invoiceQtyProductLimit = receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty');
      }else{
    	  invoiceQtyProductLimit = receipt.get('custDisBankBinPromotion').get('custdisProductLines').length;
      }
      for (var i = 0; i < receipt.attributes.lines.models.length; i++) {
             var flag = false;
                  if (!OB.UTIL.isNullOrUndefined(receipt.get('custDisBankBinPromotion').get('custdisProductLines'))) {
                     lineid = receipt.get('custDisBankBinPromotion').get('custdisProductLines').filter((item) => item.line === receipt.attributes.lines.models[i].get('id'));
                  }  
                  if (lineid.length !== 0) {
                   for(var j=0; j < invoiceQtyProductLimit; j++) { 
                    if(!OB.UTIL.isNullOrUndefined(receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].discountpercentage) && 
                      receipt.attributes.lines.models[i].get('id') === receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].line) {
                     flag = true; 
                    } 
                   }                    
                  }             
             if(flag) {
                prodCatDiscount = true; 
               }
     }
      var maxDiscountAmount = this.args.receipt.get('custdisMaxDiscountAmt');            
      for(var i=0; i < receipt.attributes.lines.models.length; i++) {
       var applicableDiscountPercentage = 0;
          if (!OB.UTIL.isNullOrUndefined(receipt.get('custDisBankBinPromotion').get('custdisProductLines'))) {
              lineid = receipt.get('custDisBankBinPromotion').get('custdisProductLines').filter((item) => item.line === receipt.attributes.lines.models[i].get('id'));
          }
          if (lineid.length !== 0) {
              for (var j = 0; j < invoiceQtyProductLimit; j++) {
                  if (receipt.attributes.lines.models[i].get('id') === receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].line) {
                      applicableDiscountPercentage = prodCatDiscount ? receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].discountpercentage : receipt.get('custdisDiscountPercentage'); 
                  }
              }
          }         
          if (i == 0) {
              cumulativeAmount[i] = receipt.attributes.lines.models[0].get('discountedGross');
              if (applicableDiscountPercentage > 0) {
                  discountCumulative[i] = receipt.attributes.lines.models[i].get('discountedGross');
              } else {
                  discountCumulative[i] = 0;
              }
              if (applicableDiscountPercentage > 0) {
                  if (grossAmt - discountCumulative[i] > 0) {
                      apportionedAmount[i] = receipt.attributes.lines.models[i].get('discountedGross');
                  } else {
                      apportionedAmount[i] = grossAmt;
                  }
              } else {
                  apportionedAmount[i] = 0;
              }
              apportionedCumulative[i] = apportionedAmount[i];
          } else {
              cumulativeAmount[i] = cumulativeAmount[i - 1] + receipt.attributes.lines.models[i].get('discountedGross');
              if (applicableDiscountPercentage > 0) {
                  discountCumulative[i] = receipt.attributes.lines.models[i].get('discountedGross') + discountCumulative[i - 1];
              } else {
                  discountCumulative[i] = discountCumulative[i - 1];
              }
              if (grossAmt - apportionedCumulative[i - 1] > 0) {
                  if (applicableDiscountPercentage > 0) {
                      if (grossAmt - discountCumulative[i] > 0) {
                          apportionedAmount[i] = receipt.attributes.lines.models[i].get('discountedGross');
                      } else {
                          apportionedAmount[i] = grossAmt - discountCumulative[i - 1];
                      }
                  } else {
                      apportionedAmount[i] = 0;
                  }
              } else {
                  apportionedAmount[i] = 0;
              }
              apportionedCumulative[i] = apportionedAmount[i] + apportionedCumulative[i - 1];
          }
          //calculating linediscountamount, then based on country making tofixed ,then checking that 
          //linediscount amount is >= maximum discount amount then setting in line level then adding total discount.
          discountAmount[i] = (applicableDiscountPercentage/100) * apportionedAmount[i];
          if(discountAmount[i] != 0){
          if(OB.MobileApp.model.get('currency').standardPrecision > 2){
        	  discountAmount[i] = +(discountAmount[i]. toFixed(3));
          }else{
        	  discountAmount[i] = +(discountAmount[i]. toFixed(2));
          }
          if(discountAmount[i] >= maxDiscountAmount){
        	  this.args.receipt.attributes.lines.models[i].set('custdisLinedisamt',maxDiscountAmount);
        	  discountAmount[i] = maxDiscountAmount;
         }else{
       	  this.args.receipt.attributes.lines.models[i].set('custdisLinedisamt',discountAmount[i]);
       	  if(OB.MobileApp.model.get('currency').standardPrecision > 2){
          	  maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(3));
    	  }else{
          	  maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(2));
    	  }
         }
          if(OB.MobileApp.model.get('currency').standardPrecision > 2){
              discount += +(discountAmount[i].toFixed(3));
          }else{
              discount += +(discountAmount[i]. toFixed(2));
          }
          }
          if(discount >= this.args.receipt.get('custdisMaxDiscountAmt')){
          	break;
          }
       }
      if(!OB.UTIL.isNullOrUndefined(receipt.get('custdisMinPurchaseAmountBankBin'))){
          receipt.set('custdisMindisamt' , receipt.get('custdisMinPurchaseAmountBankBin'));
      }     
      
      if (discount > receipt.get('custdisMaxDiscountAmt')) {
         discount = receipt.get('custdisMaxDiscountAmt');
      }
	  amount = amount - discount;
      receipt.set('custdisApplicableDiscountAmount', discount);
    }

    this.$.header.setContent(
      refund
        ? OB.I18N.getLabel('OBPOS_LblModalReturn', [
            OB.I18N.formatCurrency(amount)
          ])
        : OB.I18N.getLabel('OBPOS_LblModalPayment', [
            OB.I18N.formatCurrency(amount)
          ])
    );
    this.$.bodyContent.$.lblType.setContent(
      OB.I18N.getLabel('OBPOS_LblModalType')
    );
    this.$.bodyContent.$.paymenttype.setContent(
      providerGroup.provider._identifier
    );
    this.$.bodyContent.$.description.setContent(
      providerGroup.provider.description
    );

    _.each(receipt.get('originalPayments'), function(payment) {
        if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.properties)) {
        	if(providerGroup.provider.cpseftProviderCode == 'integrated.snbaubank' && refund){
         	   paymnetKindDuringSale = payment.kind;
               if(payment.kind === receipt.attributes.selectedPayment){
             	  if(!OB.UTIL.isNullOrUndefined(receipt.get('cpseftAurefundamt'))){
                		refundedAmtSnb = Number(receipt.get('cpseftAurefundamt')); 
                		 }
                       alredyAddedAmtInPresentReceipt = receipt.get('payment');        
                       refundedAmtSnb = Number(refundedAmtSnb-alredyAddedAmtInPresentReceipt);                 	 
                		 refundAmt = me.args.amount; 
        			if(refundAmt > refundedAmtSnb){
        				isOverPaymnet = true;
        			}else{
        				isOverPaymnet = false;
        	          remainingRefundAmt = (refundedAmtSnb - refundAmt).toFixed(2);
        	    	receipt.set('remainingAurefundamt',remainingRefundAmt);
        	}
        		}
            }
        } 
     });
if(paymnetKindDuringSale === receipt.attributes.selectedPayment){
     if(isOverPaymnet && refund){
     	this.hide();
     	OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'),OB.I18N.getLabel('CPSEFT_AUOverpaymentError',[refundedAmtSnb])
 	        );
 	         return false;
     }else if (providerGroup.provider.cpseftProviderCode == 'integrated.snbaubank' && refund){
     	this.hide();
     	var me = this;
     	OB.MobileApp.view.$.containerWindow.showPopup(
                 'OB_UI_IntegratedCreditCard',
     	      {
                 me:me,
     	      	args: me.args
     	      }
            );
     	return false;
 } 
}
    
    // Set timeout needed because on ExecuteOnShow
    setTimeout(this.startPaymentRefund.bind(this), 0);
  },
  startPaymentRefund: function() {
    var receipt = this.args.receipt;
    var amount = this.args.amount;
    var refund = this.args.refund;
    var currency = this.args.currency;
    var providerGroup = this.args.providerGroup;
    var providerinstance = this.args.providerinstance;
    var attributes = this.args.attributes;
    var i;
    const SUCCESS = "0";

    this.$.bodyContent.$.providergroupcomponent.destroyComponents();
    if (providerinstance.providerComponent) {
      this.$.bodyContent.$.providergroupcomponent
        .createComponent(providerinstance.providerComponent)
        .render();
    }

    if (providerinstance.checkOverpayment && !refund) {
      // check over payments in all payments of the group
      for (i = 0; i < providerGroup._payments.length; i++) {
        var payment = providerGroup._payments[i];

        if (!payment.paymentMethod.allowoverpayment) {
          this.showMessageAndClose(
            OB.I18N.getLabel('OBPOS_OverpaymentNotAvailable')
          );
          return;
        }

        if (
          _.isNumber(payment.paymentMethod.overpaymentLimit) &&
          amount >
            (
              receipt.get('gross') +
              payment.paymentMethod.overpaymentLimit -
              receipt.get('payment')
            ).toFixed(OB.MobileApp.model.get('currency').pricePrecision)
        ) {
          this.showMessageAndClose(
            OB.I18N.getLabel('OBPOS_OverpaymentExcededLimit')
          );
          return;
        }
      }
    }
    
    var originalCreditCardNum = null;
    var originalApprovalCode = null;
    var startDate = null;
    var rrn = null;
    var cardType = null;
    _.each(receipt.get('originalPayments'), function(payment) {
       if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.properties)) {
          originalCreditCardNum = payment.paymentData.properties.cardNumber;
          originalApprovalCode = payment.paymentData.properties.approvalCode;
          if(providerGroup.provider.cpseftProviderCode == 'integrated.snbaubank' && refund){
        	  startDate =  payment.paymentData.properties.startDate;
        	  rrn  = payment.paymentData.properties.sequenceNumber;
        	  cardType = payment.paymentData.properties.cardlogo;
          }
       } 
    });
    
    if (refund && (OB.UTIL.isNullOrUndefined(originalApprovalCode) || 
            OB.UTIL.isNullOrUndefined(originalCreditCardNum))) {
           OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'),
              OB.I18N.getLabel('CPSEFT_RefundCardDetailsMissing')
           );
           this.doHideThisPopup();
           return;
       }

    providerinstance
      .processPayment({
        receipt: receipt,
        currency: currency,
        amount: amount,
        refund: refund,
        providerGroup: providerGroup,
        originalCreditCardNum: originalCreditCardNum,
        originalApprovalCode: originalApprovalCode,
        startDate: startDate,
        rrn: rrn,
        cardType: cardType
      })
      .then(response => {
        console.log('response from device for adding payment is:' + response);
		  if (this.args.providerGroup.provider.cpseftProviderCode == 'integrated.adyen') {
			if(response.properties.adyenResponse.SaleToPOIResponse.PaymentResponse.Response.Result == 'Success'){
			  for (i = 0; i < providerGroup._payments.length; i++) {
				  const payment = providerGroup._payments[i];
				  const paymentline = {
					  kind: payment.payment.searchKey,
					  name: payment.payment._identifier,
					  amount: response.properties.processedAmount,
					  rate: payment.rate,
					  mulrate: payment.mulrate,
					  isocode: payment.isocode,
					  allowOpenDrawer: payment.paymentMethod.allowopendrawer,
					  isCash: payment.paymentMethod.iscash,
					  openDrawer: payment.paymentMethod.openDrawer,
					  printtwice: payment.paymentMethod.printtwice,
					  paymentData: {
						  provider: providerGroup.provider,
						  voidConfirmation: 'false',
						  // Is the void provider in charge of defining confirmation.
						  transaction: response.transaction,
						  authorization: response.authorization,
						  properties: response.properties,
						  data: {
							  // Set same as below in case bank bin is needed
						  }
					  },
					  eftCardDetails: response.properties.adyenResponse.SaleToPOIResponse.PaymentResponse.PaymentResult.PaymentInstrumentData.CardData.MaskedPan,
					  eftCardApprovalCode: response.properties.adyenResponse.SaleToPOIResponse.PaymentResponse.PaymentResult.PaymentAcquirerData.ApprovalCode
				  };
				  receipt.addPayment(
					  new OB.Model.PaymentLine(Object.assign(paymentline, attributes))
				  );
				  window.setTimeout(this.doHideThisPopup.bind(this), 0);
			  }
			} else {
				// to add payment to ticket only if device response is success
				this.showMessageAndClose(OB.I18N.getLabel('OBPOS_TransactionError'));
			}

		  } else if (response.resultCode !== SUCCESS ) {
          // to add payment to ticket only if device response is success
          this.showMessageAndClose(OB.I18N.getLabel('OBPOS_TransactionError'));
        } else {
          // setting void properties
          response.properties.paymentProperties = {};
          response.properties.paymentProperties.ecrReceiptNumber =
            response.properties.ecrReceiptNumber;
          response.properties.paymentProperties.sequenceNumber =
            response.properties.sequenceNumber;
          response.properties.paymentProperties.approvalCode =
            response.properties.approvalCode;
          response.properties.paymentProperties.cardNumber =
            response.properties.cardNumber;
			  response.properties.paymentProperties.providerCode =
				  response.request.properties.providerCode;
			  response.properties.paymentProperties.invoiceNumber =
				  response.properties.invoiceNumber;
			  response.properties.paymentProperties.EazypayTerminalId =
				  localStorage.getItem('eazypayTerminalId');
			  if(this.args.providerGroup.provider.cpseftProviderCode == 'integrated.eazypay' || this.args.providerGroup.provider.cpseftProviderCode == 'integrated.afsbank'){
			  response.properties.paymentProperties.posInvoiceForEazypay =
				  response.properties.PosInvoice;
			  response.properties.paymentProperties.paxRrn =
				  response.properties.PAX_RRN;
			  }else if(this.args.providerGroup.provider.cpseftProviderCode == 'integrated.snbaubank'){
				  response.properties.paymentProperties.startDate =
					  response.properties.startDate;
			  }

          
          var mainPaymentMethod = false;
          const addResponseToPayment = payment => {
            // We found the payment method that applies.
            const paymentline = {
              kind: payment.payment.searchKey,
              name: payment.payment._identifier,
              amount: response.request.amount,
              rate: payment.rate,
              mulrate: payment.mulrate,
              isocode: payment.isocode,
              allowOpenDrawer: payment.paymentMethod.allowopendrawer,
              isCash: payment.paymentMethod.iscash,
              openDrawer: payment.paymentMethod.openDrawer,
              printtwice: payment.paymentMethod.printtwice,
              paymentData: {
                provider: providerGroup.provider,
                voidConfirmation: false,
                // Is the void provider in charge of defining confirmation.
                transaction: response.transaction,
                authorization: response.authorization,
                properties: response.properties,
                data: {
                   custdisMainPaymentMethod: mainPaymentMethod,
                   custdisAppliedPaymentMethod: payment.payment._identifier,
                   posInvoiceForEazypay : response.properties.PosInvoice,
                   paxRrn : response.properties.PAX_RRN
                }   
              },
              eftCardDetails: response.properties.cardNumber,
              eftCardApprovalCode: response.properties.approvalCode,
              eftCardType:response.properties.cardlogo,
              eftRrn:response.properties.sequenceNumber
            };
            receipt.addPayment(
              new OB.Model.PaymentLine(Object.assign(paymentline, attributes))
            );
            window.setTimeout(this.doHideThisPopup.bind(this), 0);
          };
    
          // First attempt. Find an exact match.
          var me = this;
          const cardlogo = response.properties.cardlogo;
          const cardNumber = response.properties.cardNumber;
          let undefinedPayment = null;
          
          if (receipt.get('custdisBankBinDiscountApplied') && (receipt.get('custdisCardBinNumber') !== cardNumber.slice(0,6) || receipt.get('custdisCardNumber') !== cardNumber.slice(cardNumber.length-4, cardNumber.length))) {
             receipt.set('custdisBankBinDiscountApplied', false);
             OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_BinNumberNotMatching'));
          }
          for (i = 0; i < providerGroup._payments.length; i++) {
            const payment = providerGroup._payments[i];
            if ((cardNumber.startsWith('34') || cardNumber.startsWith('37')) && 
                 'AMEX' === payment.paymentType.searchKey) {
              if (receipt.get('custdisBankBinDiscountApplied')) {
                mainPaymentMethod = true;
                addResponseToPayment(payment);
                receipt.addPayment(new OB.Model.PaymentLine({
                       'kind': receipt.get('custdisLinkedPaymentMethod').paymentMethod.searchKey,
                       'name': receipt.get('custdisLinkedPaymentMethod').paymentMethod.name,
                       'amount': receipt.get('custdisApplicableDiscountAmount'),
                       'allowOpenDrawer': receipt.get('custdisLinkedPaymentMethod').paymentMethod.allowopendrawer,
                       'isCash': receipt.get('custdisLinkedPaymentMethod').paymentMethod.iscash,
                       'openDrawer': receipt.get('custdisLinkedPaymentMethod').paymentMethod.openDrawer,
                       'isocode': receipt.get('custdisLinkedPaymentMethod').isocode,
                       'printtwice': receipt.get('custdisLinkedPaymentMethod').paymentMethod.printtwice,
                       'paymentData': {
                            data: {
                                custdisLinkedPaymentMethod: true,
                                custdisAppliedPaymentMethod: '"' + payment.payment._identifier + '"'
                            },
                            CardNumber: ''
                        }
                 }));
             
                _.each(receipt.get('lines').models, function(line) {
                    _.each(receipt.get('custDisBankBinPromotion').get('custdisProductLines'), function(promoProducts) {
                        if (line.get('id') === promoProducts.line && !OB.UTIL.isNullOrUndefined(line.get('custdisLinedisamt'))) {
                           receipt.addPromotion(line, receipt.get('custDisBankBinPromotion'), {amt:0});
                           line.set('custdisBankBinOffer', receipt.get('custDisBankBinPromotion').get('id')); 
                        }   
                    });                  
                });
                receipt.set('custdisBankBinDiscountApplied', false); 
                receipt.unset('custdisLinkedPaymentMethod');
                receipt.unset('custDisBankBinPromotion');
                receipt.unset('custdisBankBinDiscountApplied');
                receipt.unset('custdisCardBinNumber');
                receipt.unset('custdisCardNumber');
                receipt.unset('custdisMaxDiscountAmt');
                receipt.unset('custdisDiscountPercentage');
                receipt.unset('custdisMinPurchaseAmountBankBin');
                receipt.unset('custdisSelectedLineAmount'); 
                receipt.save(); 
              } else {
                addResponseToPayment(payment);
              }
              return; // Success
            } else if (cardlogo === payment.paymentType.searchKey && !(cardNumber.startsWith('34') || cardNumber.startsWith('37'))) {
              if (receipt.get('custdisBankBinDiscountApplied')) {
                mainPaymentMethod = true;
                addResponseToPayment(payment);
                receipt.addPayment(new OB.Model.PaymentLine({
                       'kind': receipt.get('custdisLinkedPaymentMethod').paymentMethod.searchKey,
                       'name': receipt.get('custdisLinkedPaymentMethod').paymentMethod.name,
                       'amount': receipt.get('custdisApplicableDiscountAmount'),
                       'allowOpenDrawer': receipt.get('custdisLinkedPaymentMethod').paymentMethod.allowopendrawer,
                       'isCash': receipt.get('custdisLinkedPaymentMethod').paymentMethod.iscash,
                       'openDrawer': receipt.get('custdisLinkedPaymentMethod').paymentMethod.openDrawer,
                       'printtwice': receipt.get('custdisLinkedPaymentMethod').paymentMethod.printtwice,
                       'isocode': receipt.get('custdisLinkedPaymentMethod').isocode,
                       'paymentData': {
                            data: {
                                custdisLinkedPaymentMethod: true,
                                custdisAppliedPaymentMethod: payment.payment._identifier
                            },
                            CardNumber: ''
                        }
                 }));
                 
                _.each(receipt.get('lines').models, function(line) {
                    _.each(receipt.get('custDisBankBinPromotion').get('custdisProductLines'), function(promoProducts) {
                        if (line.get('id') === promoProducts.line && !OB.UTIL.isNullOrUndefined(line.get('custdisLinedisamt'))) {
                           receipt.addPromotion(line, receipt.get('custDisBankBinPromotion'), {amt:0});
                           line.set('custdisBankBinOffer', receipt.get('custDisBankBinPromotion').get('id')); 
                        }   
                    });                  
                });
                receipt.set('custdisBankBinDiscountApplied', false); 
                receipt.unset('custdisLinkedPaymentMethod');
                receipt.unset('custDisBankBinPromotion');
                receipt.unset('custdisBankBinDiscountApplied');
                receipt.unset('custdisCardBinNumber');
                receipt.unset('custdisCardNumber');
                receipt.unset('custdisMaxDiscountAmt');
                receipt.unset('custdisDiscountPercentage');
                receipt.unset('custdisMinPurchaseAmountBankBin');
                receipt.unset('custdisSelectedLineAmount'); 
                receipt.save(); 
              } else {
                addResponseToPayment(payment);
              }
              return; // Success
            } else if ('UNDEFINED' === payment.paymentType.searchKey) {
              undefinedPayment = payment;
            }
          }

          // Second attempt. Find UNDEFINED paymenttype.
          if (undefinedPayment) {
            if (receipt.get('custdisBankBinDiscountApplied')) {
              mainPaymentMethod = true;
              addResponseToPayment(undefinedPayment);
              receipt.addPayment(new OB.Model.PaymentLine({
                       'kind': receipt.get('custdisLinkedPaymentMethod').paymentMethod.searchKey,
                       'name': receipt.get('custdisLinkedPaymentMethod').paymentMethod.name,
                       'amount': receipt.get('custdisApplicableDiscountAmount'),
                       'allowOpenDrawer': receipt.get('custdisLinkedPaymentMethod').paymentMethod.allowopendrawer,
                       'isCash': receipt.get('custdisLinkedPaymentMethod').paymentMethod.iscash,
                       'openDrawer': receipt.get('custdisLinkedPaymentMethod').paymentMethod.openDrawer,
                       'printtwice': receipt.get('custdisLinkedPaymentMethod').paymentMethod.printtwice,
                       'isocode': receipt.get('custdisLinkedPaymentMethod').isocode,
                       'paymentData': {
                            data: {
                                custdisLinkedPaymentMethod: true,
                                custdisAppliedPaymentMethod: undefinedPayment.payment._identifier
                            },
                            CardNumber: ''
                        }
                 }));
              _.each(receipt.get('lines').models, function(line) {
                    _.each(receipt.get('custDisBankBinPromotion').get('custdisProductLines'), function(promoProducts) {
                        if (line.get('id') === promoProducts.line && !OB.UTIL.isNullOrUndefined(line.get('custdisLinedisamt'))) {
                           receipt.addPromotion(line, receipt.get('custDisBankBinPromotion'), {amt:0});
                           line.set('custdisBankBinOffer', receipt.get('custDisBankBinPromotion').get('id')); 
                        }   
                   });                  
               });
               receipt.set('custdisBankBinDiscountApplied', false); 
               receipt.unset('custdisLinkedPaymentMethod');
               receipt.unset('custDisBankBinPromotion');
               receipt.unset('custdisBankBinDiscountApplied');
               receipt.unset('custdisCardBinNumber');
               receipt.unset('custdisCardNumber');
               receipt.unset('custdisMaxDiscountAmt');
               receipt.unset('custdisDiscountPercentage');
               receipt.unset('custdisMinPurchaseAmountBankBin');
               receipt.unset('custdisSelectedLineAmount');  
               receipt.save();
            } else {
              addResponseToPayment(undefinedPayment);
            }
            return; // Success
          }

          // Fail. Cannot find payment to assign response
          this.showMessageAndClose(
            OB.I18N.getLabel('OBPOS_CannotFindPaymentMethod')
          );
        }
      })
      .catch(exception => {
        this.showMessageAndClose(
          providerinstance.getErrorMessage
            ? providerinstance.getErrorMessage(exception)
            : exception.message
        );
      });
  }
});

// ****************** extending OB.UI.ModalProviderGroupVoid ******************* //
/* global $, _ */

OB.UI.ModalProviderGroupVoid.extend({
  executeOnShow: function() {
    var me = this; 
    var receipt = this.args.receipt;
    var isLinkedPaymentPresent = false;   
    receipt.get('payments').models.forEach(function (payment) {
      if (!OB.UTIL.isNullOrUndefined(payment.get('paymentData')) && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data) 
          && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data.custdisLinkedPaymentMethod)
          && payment.get('paymentData').data.custdisLinkedPaymentMethod) {
            me.showMessageAndClose(
              OB.I18N.getLabel('CUSTDIS_CannotDeleteLinkedPayment')
            );
            isLinkedPaymentPresent = true;              
      } 
    });
    
    if (isLinkedPaymentPresent) {
      this.doHideThisPopup();
      return false;
    }
    
    if (receipt.get('eftPaymentIsNegative')) {
      this.showMessageAndClose(
         OB.I18N.getLabel('CPSEFT_RefundVoidMessage')
      );
      this.doHideThisPopup();
      return false;
    }
    
    var payment = this.args.payment;
    var amount = payment.get('amount');
    var provider = payment.get('paymentData').provider;

    this.$.header.setContent(
      OB.I18N.getLabel('OBPOS_LblModalVoidTransaction', [
        OB.I18N.formatCurrency(amount)
      ])
    );
    this.$.bodyContent.$.lblType.setContent(
      OB.I18N.getLabel('OBPOS_LblModalType')
    );
    this.$.bodyContent.$.paymenttype.setContent(provider._identifier);
    this.$.bodyContent.$.description.setContent(provider.description);

    // Set timeout needed because on ExecuteOnShow
    setTimeout(this.startVoid.bind(this), 0);
  },
  startVoid: function() {
    var payment = this.args.payment;
    var amount = payment.get('amount');
    var provider = payment.get('paymentData').provider;
    var providerinstance = this.args.providerinstance;

    var receipt = this.args.receipt;
    var removeTransaction = this.args.removeTransaction;
    const SUCCESS = "0";
    
    this.$.bodyContent.$.providergroupcomponent.destroyComponents();
    if (providerinstance.providerComponent) {
      this.$.bodyContent.$.providergroupcomponent
        .createComponent(providerinstance.providerComponent)
        .render();
    }

    providerinstance
      .processVoid({
        receipt: receipt,
        payment: payment
      })
      .then(
        function(response) {
          console.log(
            'response from device for voiding the NI payment is:' + response
          );
			  if (provider.cpseftProviderCode == 'integrated.adyen') {
				  removeTransaction();
				  window.setTimeout(this.doHideThisPopup.bind(this), 0);
			  } else if (response.resultCode !== SUCCESS) {
            // To void the payment only if device response is success
            this.showMessageAndClose(
              OB.I18N.getLabel('OBPOS_TransactionError')
            );
          } else {
            removeTransaction();
            window.setTimeout(this.doHideThisPopup.bind(this), 0);
          }
        }.bind(this)
      )
      ['catch'](
        function(exception) {
          this.showMessageAndClose(
            providerinstance.getErrorMessage
              ? providerinstance.getErrorMessage(exception)
              : exception.message
          );
        }.bind(this)
      );
  }
});

// ******************* Extnding OBPOS_StandardProvider ********************* //

OBPOS_StandardProvider.extend({
  processPayment: function(paymentinfo) {
    /*var type = paymentinfo.refund
      ? OBPOS_StandardProvider.TYPE_REFUND
      : OBPOS_StandardProvider.TYPE_SALE;*/
	  var type;
    if(paymentinfo.refund){
    	type = OBPOS_StandardProvider.TYPE_REFUND;
    }else if(paymentinfo.providerGroup.provider.cpseftProviderCode === 'integrated.mashreq.upi' && !paymentinfo.refund){
    	type = '11';
    }else{
    	type = OBPOS_StandardProvider.TYPE_SALE;
    }
	var discount = 0.0;
	if (paymentinfo.receipt.get('custdisBankBinDiscountApplied')) {
	  var custdisSelectedLineAmount = paymentinfo.receipt.get('custdisSelectedLineAmount'); 
	  var grossAmt = paymentinfo.amount;
      if (paymentinfo.receipt.get('custdisMinPurchaseAmountBankBin') > grossAmt) {
         var symbol = OB.MobileApp.model.get('terminal').symbol;
         var symbolAtRight = OB.MobileApp.model.get('terminal').currencySymbolAtTheRight;
         OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningTitle'), OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningBody', [OB.I18N.formatCurrencyWithSymbol(paymentinfo.receipt.get('custdisMinPurchaseAmountBankBin'), symbol, symbolAtRight)]));
         return false;
      }
      var discountAmount = new Array();
      var prodCatDiscount = false;
      var cumulativeAmount = new Array();
      var apportionedAmount = new Array();
      var apportionedCumulative = new Array();
      var discountCumulative = new Array();      
      var prodCatDiscount = false; 
      var invoicePerQty = 0;
      if(!OB.UTIL.isNullOrUndefined(paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty')) && paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty') < paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines').length){
    	  invoicePerQty =  paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty');
      }else{
    	  invoicePerQty =  paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines').length;
      }
      for (var i = 0; i < paymentinfo.receipt.attributes.lines.models.length; i++) {
             var flag = false;
                  if (!OB.UTIL.isNullOrUndefined(paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines'))) {
                     lineid = paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines').filter((item) => item.line === paymentinfo.receipt.attributes.lines.models[i].get('id'));
                  }  
                  if (lineid.length !== 0) {
                   for(var j=0; j < invoicePerQty; j++) { 
                    if(!OB.UTIL.isNullOrUndefined(paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].discountpercentage) && 
                      paymentinfo.receipt.attributes.lines.models[i].get('id') === paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].line) {
                     flag = true; 
                    } 
                   }                    
                  }             
             if(flag) {
                prodCatDiscount = true; 
               }
     }
      var maxDiscountAmount = paymentinfo.receipt.get('custdisMaxDiscountAmt');            
      for(var i=0; i < paymentinfo.receipt.attributes.lines.models.length; i++) {
       var applicableDiscountPercentage = 0;
          if (!OB.UTIL.isNullOrUndefined(paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines'))) {
              lineid = paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines').filter((item) => item.line === paymentinfo.receipt.attributes.lines.models[i].get('id'));
          }
          if (lineid.length !== 0) {
              for (var j = 0; j < invoicePerQty; j++) {
                  if (paymentinfo.receipt.attributes.lines.models[i].get('id') === paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].line) {
                      applicableDiscountPercentage = prodCatDiscount ? paymentinfo.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].discountpercentage : paymentinfo.receipt.get('custdisDiscountPercentage'); 
                  }
              }
          }         
          if (i == 0) {
              cumulativeAmount[i] = paymentinfo.receipt.attributes.lines.models[0].get('discountedGross');
              if (applicableDiscountPercentage > 0) {
                  discountCumulative[i] = paymentinfo.receipt.attributes.lines.models[i].get('discountedGross');
              } else {
                  discountCumulative[i] = 0;
              }
              if (applicableDiscountPercentage > 0) {
                  if (grossAmt - discountCumulative[i] > 0) {
                      apportionedAmount[i] = paymentinfo.receipt.attributes.lines.models[i].get('discountedGross');
                  } else {
                      apportionedAmount[i] = grossAmt;
                  }
              } else {
                  apportionedAmount[i] = 0;
              }
              apportionedCumulative[i] = apportionedAmount[i];
          } else {
              cumulativeAmount[i] = cumulativeAmount[i - 1] + paymentinfo.receipt.attributes.lines.models[i].get('discountedGross');
              if (applicableDiscountPercentage > 0) {
                  discountCumulative[i] = paymentinfo.receipt.attributes.lines.models[i].get('discountedGross') + discountCumulative[i - 1];
              } else {
                  discountCumulative[i] = discountCumulative[i - 1];
              }
              if (grossAmt - apportionedCumulative[i - 1] > 0) {
                  if (applicableDiscountPercentage > 0) {
                      if (grossAmt - discountCumulative[i] > 0) {
                          apportionedAmount[i] = paymentinfo.receipt.attributes.lines.models[i].get('discountedGross');
                      } else {
                          apportionedAmount[i] = grossAmt - discountCumulative[i - 1];
                      }
                  } else {
                      apportionedAmount[i] = 0;
                  }
              } else {
                  apportionedAmount[i] = 0;
              }
              apportionedCumulative[i] = apportionedAmount[i] + apportionedCumulative[i - 1];
          }
          
          //calculating linediscountamount, then based on country making tofixed ,then checking that 
          //linediscount amount is >= maximum discount amount then setting in line level then adding total discount.
          discountAmount[i] = (applicableDiscountPercentage/100) * apportionedAmount[i];
          if(discountAmount[i] != 0){
          if(OB.MobileApp.model.get('currency').standardPrecision > 2){
        	  discountAmount[i] = +(discountAmount[i].toFixed(3));
          }else{
        	  discountAmount[i] = +(discountAmount[i].toFixed(2));
          }
          if(discountAmount[i] >= maxDiscountAmount){
        	  paymentinfo.receipt.attributes.lines.models[i].set('custdisLinedisamt',maxDiscountAmount);
        	  discountAmount[i] = maxDiscountAmount;
         }else{
        	 paymentinfo.receipt.attributes.lines.models[i].set('custdisLinedisamt',discountAmount[i]);
       	  if(OB.MobileApp.model.get('currency').standardPrecision > 2){
          	  maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(3));
    	  }else{
          	  maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(2));
    	  }
         }
          if(OB.MobileApp.model.get('currency').standardPrecision > 2){
              discount += +(discountAmount[i].toFixed(3));
          }else{
              discount += +(discountAmount[i].toFixed(2));
          }
          }
          if(discount >= paymentinfo.receipt.get('custdisMaxDiscountAmt')){
          	break;
          }
       }
      if(!OB.UTIL.isNullOrUndefined(paymentinfo.receipt.get('custdisMinPurchaseAmountBankBin'))){
    	  paymentinfo.receipt.set('custdisMindisamt' , paymentinfo.receipt.get('custdisMinPurchaseAmountBankBin'));
      }

     
      if (discount > paymentinfo.receipt.get('custdisMaxDiscountAmt')) {
         discount = paymentinfo.receipt.get('custdisMaxDiscountAmt');
      }
	  paymentinfo.amount = paymentinfo.amount - discount;
      paymentinfo.receipt.set('custdisApplicableDiscountAmount', discount);
    }
    
    var request = {
      type: type,
      currency: paymentinfo.currency,
      amount: paymentinfo.amount,
      terminal: localStorage.getItem('mashreqTerminalId'),
      properties: {
        provider: paymentinfo.providerGroup.provider.provider,
        providerCode: paymentinfo.providerGroup.provider.cpseftProviderCode,
        originalCreditCardNum: paymentinfo.originalCreditCardNum,
        originalApprovalCode: paymentinfo.originalApprovalCode,
        startDate: paymentinfo.startDate,
        rrn: paymentinfo.rrn,
        cardType: paymentinfo.cardType,
        EazypayTerminalId: localStorage.getItem('eazypayTerminalId'),
      }
    };

    request = paymentinfo.refund
      ? this.populateRefundRequest(request, paymentinfo)
      : this.populatePaymentRequest(request, paymentinfo);

    return OBPOS_StandardProvider.remoteRequest(request);
  }
});

// ******************* Extending OBPOS_StandardProviderVoid ********************* //

OBPOS_StandardProviderVoid.extend({
  processVoid: function(voidpaymentinfo) {
    var request = {
      type: OBPOS_StandardProvider.TYPE_VOID,
      currency: voidpaymentinfo.payment.get('isocode'),
      amount: voidpaymentinfo.payment.get('amount'),
      terminal: localStorage.getItem('mashreqTerminalId'),
      properties: voidpaymentinfo.payment.get('paymentData').properties
        .paymentProperties
    };

    request = this.populateVoidRequest(request, voidpaymentinfo);

    return OBPOS_StandardProvider.remoteRequest(request);
  }
});
