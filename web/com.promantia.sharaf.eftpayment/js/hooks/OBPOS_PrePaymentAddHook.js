/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_preAddPayment', function (args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('paymentData')) && !OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('paymentData').provider)  
        && !OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('paymentData').provider.cpseftProviderCode)  && args.paymentToAdd.get('paymentData').provider.cpseftProviderCode.indexOf('integrated') > -1
    	&& (OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('eftCardDetails')) || args.paymentToAdd.get('eftCardDetails') === ""  || 
    	    OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('eftCardApprovalCode')) || args.paymentToAdd.get('eftCardApprovalCode') === "")) {
      OB.MobileApp.view.$.containerWindow.showPopup(
            'OB_UI_IntegratedCreditCard',
	      {
	      	args: args,
	        callbacks: callbacks
	      }
       );
    } else {
    	OB.UTIL.HookManager.callbackExecutor(args, callbacks);	
    }
  });
}());