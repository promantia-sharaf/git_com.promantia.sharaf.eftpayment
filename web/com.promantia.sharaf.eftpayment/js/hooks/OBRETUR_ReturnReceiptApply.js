/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function (args, callbacks) {
    _.each(args.order.receiptPayments, function(payment) {
        if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.properties)) {
           OB.UTIL.localStorage.setItem(args.order.documentNo, JSON.stringify(args.order.receiptPayments));
        } 
        if(!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.properties) && !OB.UTIL.isNullOrUndefined(payment.paymentData.provider.cpseftProviderCode
) && payment.paymentData.provider.cpseftProviderCode === 'integrated.snbaubank'){
        	OB.MobileApp.model.receipt.set('cpseftAurefundamt',args.order.cpseftAurefundamt);
        	
        }
    });
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);   
  });
}());