OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function(
  args,
  callbacks
) {
  new OB.DS.Process(
    'com.promantia.sharaf.eftpayment.process.GetMashreqTerminalID'
  ).exec(
    {
      posTerminalID: OB.MobileApp.model.get('terminal').id
    },
    function(data) {
      if (data && data.exception) {
        if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
          console.log(data.exception.message);
        } else {
          console.log('Unknown Exception while getting mashreq terminal ID');
        }
      } else {
        localStorage.setItem('mashreqTerminalId', (data.terminalID));
        localStorage.setItem('eazypayTerminalId', (data.eazypayTerminalId));
      }
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  );
});
