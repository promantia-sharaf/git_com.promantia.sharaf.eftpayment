/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBMOBC_PrePaymentSelected', function (args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args.receipt) && args.receipt.get('net') > 0 && !OB.UTIL.isNullOrUndefined(args.paymentSelected.permission) && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames[args.paymentSelected.permission])
        && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames[args.paymentSelected.permission].providerGroup)
        && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames[args.paymentSelected.permission].providerGroup.cpseftProviderCode) &&
        OB.MobileApp.model.paymentnames[args.paymentSelected.permission].providerGroup.cpseftProviderCode === 'integrated.mashreq') {
        
        if (!args.cancellation && !args.receipt.get('isVerifiedReturn')) {
          args.order = args.receipt;  
          args.order.set('custdisIntegratedCC', args.paymentSelected.permission);  
          OB.BANKBIN.CHECKANDAPPLY(args, callbacks);
        }
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else {
    	OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } 
  });
}());