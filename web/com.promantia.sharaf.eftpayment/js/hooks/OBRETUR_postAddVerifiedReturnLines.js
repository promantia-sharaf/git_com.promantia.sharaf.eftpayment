/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBRETUR_postAddVerifiedReturnLines', function (args, callbacks) {
    _.each(args.receipt.get('lines').models, function(line) {
        if (!OB.UTIL.isNullOrUndefined(line.get('originalDocumentNo'))) {
           var payments = OB.UTIL.localStorage.getItem(line.get('originalDocumentNo'));
           if (!OB.UTIL.isNullOrUndefined(payments)) {
              args.receipt.set('originalPayments', JSON.parse(payments)); 
              OB.UTIL.localStorage.removeItem(line.get('originalDocumentNo'));
              payments = null;
           }
        } 
    });
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);    
  });
}());