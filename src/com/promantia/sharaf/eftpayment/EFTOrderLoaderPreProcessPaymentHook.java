package com.promantia.sharaf.eftpayment;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.retail.posterminal.OrderLoaderPreProcessPaymentHook;

public class EFTOrderLoaderPreProcessPaymentHook extends OrderLoaderPreProcessPaymentHook {

  @Override
  public void exec(JSONObject jsonorder, Order order, JSONObject jsonpayment, FIN_Payment payment)
      throws Exception {

    if (jsonpayment.has("eftCardDetails")) {
      String cardDetails = jsonpayment.getString("eftCardDetails");
      payment.setSharccCardDetails(cardDetails);
    }
    if (jsonpayment.has("eftCardApprovalCode")) {
      String sharccCardAuthcode = jsonpayment.getString("eftCardApprovalCode");
      payment.setSharccCardAuthcode(sharccCardAuthcode);
    }
    if (jsonpayment.has("eftCardType") && jsonpayment.has("eftRrn")) {
      String sharccCardtype = jsonpayment.getString("eftCardType");
      String sharccCardrrn = jsonpayment.getString("eftRrn");
      payment.setCpseftCardType(sharccCardtype);
      payment.setCpseftRrn(sharccCardrrn);
    }

  }

}
