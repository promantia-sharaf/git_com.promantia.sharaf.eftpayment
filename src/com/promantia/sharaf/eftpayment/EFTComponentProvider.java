package com.promantia.sharaf.eftpayment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

@ApplicationScoped
@ComponentProvider.Qualifier(EFTComponentProvider.QUALIFIER)
public class EFTComponentProvider extends BaseComponentProvider {

  public static final String QUALIFIER = "CPSEFT_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.promantia.sharaf.eftpayment";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    String[] resourceList = { "hooks/OBPOS_TerminalLoadedFromBackend",
        "providers/extendStandardProvider", "hooks/OBPOS_PrePaymentAddHook",
        "components/creditCardModal", "providers/extendModalPayment",
        "hooks/OBMOBC_PrePaymentSelectedHook", "hooks/OBRETUR_postAddVerifiedReturnLines",
        "hooks/OBRETUR_ReturnReceiptApply" };

    for (String resource : resourceList) {
      globalResources.add(createComponentResource(ComponentResourceType.Static,
          prefix + resource + ".js", POSUtils.APP_NAME));
    }

    return globalResources;
  }

}
