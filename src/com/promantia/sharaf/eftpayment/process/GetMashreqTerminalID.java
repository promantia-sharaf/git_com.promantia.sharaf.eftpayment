package com.promantia.sharaf.eftpayment.process;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.retail.posterminal.OBPOSApplications;

public class GetMashreqTerminalID extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(GetMashreqTerminalID.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();

    try {
      // Send the configured mashreq terminal ID from POS Terminal Window to Web POS
      OBContext.setAdminMode(true);

      OBPOSApplications posTerminal = OBDal.getInstance().get(OBPOSApplications.class,
          jsonsent.get("posTerminalID"));

      data.put("terminalID", posTerminal.getCpseftTerminalid());
      data.put("eazypayTerminalId", posTerminal.getCpseftEazypayid());
      result.put("status", 0);
      result.put("data", data);
    } catch (Exception e) {
      log.error("Error getting Mashreq terminalID from POS Terminal: " + e.getMessage(), e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

}
