package com.promantia.sharaf.eftpayment;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.posterminal.OrderLoaderHook;

public class EFTOrderLoaderHook implements OrderLoaderHook {
  private static final Logger log = Logger.getLogger(EFTOrderLoaderHook.class);

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {
    try {
      boolean isVerifiedReturn = jsonorder.has("isVerifiedReturn")
          ? jsonorder.getBoolean("isVerifiedReturn")
          : false;
      String cpseftProviderCode = null;
      String orderID = null;
      String documentNo = null;
      String aurefundAmt = null;
      String saleAuAmt = null;
      if (jsonorder.has("remainingAurefundamt")
          && jsonorder.getString("remainingAurefundamt") != null) {
        aurefundAmt = jsonorder.getString("remainingAurefundamt");
      }
      JSONArray linesArray = jsonorder.getJSONArray("lines");
      JSONArray paymentsArray = jsonorder.getJSONArray("payments");
      for (int i = 0; i < paymentsArray.length(); i++) {
        JSONObject payment = paymentsArray.getJSONObject(i);
        if (payment.has("paymentData")) {
          JSONObject paymentData = payment.getJSONObject("paymentData");
          if (paymentData.has("provider")) {
            JSONObject provider = paymentData.getJSONObject("provider");
            if (provider.has("cpseftProviderCode")) {
              cpseftProviderCode = provider.getString("cpseftProviderCode");
              saleAuAmt = payment.getString("amount");
            }
          }
        }
      }
      if (isVerifiedReturn) {
        for (int i = 0; i < linesArray.length(); i++) {
          JSONObject line = linesArray.getJSONObject(i);
          orderID = line.getString("orderId");
        }
      }
      if (!isVerifiedReturn && cpseftProviderCode.equals("integrated.snbaubank")) {
        OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
        orderCriteria.add(Restrictions.eq(Order.PROPERTY_ID, jsonorder.getString("id")));
        Order orderObj = (Order) orderCriteria.uniqueResult();
        if (orderObj != null) {
          orderObj.setCpseftAurefundamt(new BigDecimal(saleAuAmt));
          OBDal.getInstance().save(orderObj);
          OBDal.getInstance().flush();
        }
      }
      if (isVerifiedReturn && cpseftProviderCode.equals("integrated.snbaubank")
          && orderID != null) {
        OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
        orderCriteria.add(Restrictions.eq(Order.PROPERTY_ID, orderID));
        Order orderObj = (Order) orderCriteria.uniqueResult();
        if (orderObj != null) {
          orderObj.setCpseftAurefundamt((new BigDecimal(aurefundAmt)));
          OBDal.getInstance().save(orderObj);
          OBDal.getInstance().flush();
        }
      }
    } catch (Exception e) {
      log.error("Exception while updating refund Amount of SNB/AU in EFTOrderLoaderHook.java ", e);
    }
  }

}
